a.clk_gen chip is si5338
b.this file can work on linux platform(g++-8.1) and windwos platform(msvc2017)


How to use ?

1. execute softeare Clockbuilder Desktop(silicon Lab)
    generate c-header-file

2. open this generated file.
    go line-90 :
    Reg_Data const code Reg_Store[NUM_REGS_MAX] = {
    modify as :
    Reg_Data const Reg_Store[NUM_REGS_MAX] = {
3. compile Creg2VHDLreg.cpp(use bash shell on terminal)
    if you want to use clang to compile 
    shell :
    clang Creg2VHDLreg.cpp -o Creg2VHDLreg
    or you want to use gcc to compile 
    shell :
    g++ Creg2VHDLreg.cpp -o Creg2VHDLreg
4. execut compiled file.
    shell :
    ./Creg2VHDLreg
5. if you want to show result on terminal,
    key in y , or not key in n
6. output VHDL file : VHDL_Code_Reg_map.vhd

7.  if you want to invoke this reg-map in vhdl project,
    just add below code on your VHDL code at frist line:
        library work;
        use work.VHDL_Code_Reg_map.all;
