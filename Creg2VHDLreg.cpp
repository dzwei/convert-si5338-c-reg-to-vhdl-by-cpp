#include "precompile.h"
#include "Define.h"	
#include "register_map.h"

using namespace std;

int main()
{	
	//attention
	//go to register_map.h
	//find line of  "Reg_Data const code Reg_Store[NUM_REGS_MAX] = {"
	//take out "code"
#pragma region Key output path here
	const string out_file_dir = "";
	const string out_file_name = "VHDL_Code_Reg_map";
	const string subfilename_out_file_name = ".vhd";
	const string out_file_name_tol = out_file_name + subfilename_out_file_name;
#pragma endregion

    ofstream output_file(out_file_name_tol, ios::out | ios::trunc);
	//ios::trunc ->write after delect content 
	//ios::app -> Attach the new data to the original contents of the file
    string in_str_of_show_result(1,'n');

	const string dst_package_name = out_file_name;
	const string dst_i2c_memory_name_buf = "My_i2c_memory";
	const string dst_i2c_memory_name = "My_reg_data";
#pragma region name of struct menber in VHDL code
	dst_stuct dst_stuct;
    dst_stuct.stuct_name = "data_clk_i2c";
	dst_stuct.Reg_Addr   = "Reg_Addr";
	dst_stuct.Reg_Val    = "Reg_Val";
	dst_stuct.Reg_Mask   = "Reg_Mask";
#pragma endregion
//----------------------------------------------------------------

#pragma region converter	
	cout << "converting C_reg file to VHDL  please wait ......." << endl;

		output_file << "library ieee;" << "\n";
		output_file << "use ieee.std_logic_1164.all;" << "\n\n";
		output_file << "package " << dst_package_name << " is" << "\n";
		output_file << "\ttype " << dst_stuct.stuct_name << " is record" << "\n";
		output_file <<"\t\t" << dst_stuct.Reg_Addr << " : std_logic_vector(7 downto 0);" << "\n";
		output_file <<"\t\t" << dst_stuct.Reg_Val << " : std_logic_vector(7 downto 0);" << "\n";
		output_file <<"\t\t" << dst_stuct.Reg_Mask << " : std_logic_vector(7 downto 0);" << "\n";
		output_file << "\tend record " << dst_stuct.stuct_name <<";"<<"\n"<<endl;
		output_file << "\ttype array_" << dst_stuct.stuct_name << " is array (0 to " << NUM_REGS_MAX - 2 << ") of "<< dst_stuct.stuct_name <<";\n\n";		
		output_file << "\tconstant " << dst_i2c_memory_name_buf << " : " << "array_" << dst_stuct.stuct_name << "; \n\n";
		output_file << "end package " << dst_package_name << ";\n" << endl;

		output_file << "package body " << dst_package_name << " is" << "\n";
		output_file << "\tconstant " << dst_i2c_memory_name_buf << " : " << "array_" << dst_stuct.stuct_name << " := \n";
		output_file << "\t\t(" << endl;
				for (int i = 0; i < NUM_REGS_MAX - 1; ++i) {
					output_file << "\t\t\t "<<dec <<i <<" => (" << dst_stuct.Reg_Addr <<" => x\"" << setw(2) << setfill('0') << hex << (int)Reg_Store[i].Reg_Addr << "\",";
					output_file << dst_stuct .Reg_Val<<" => x\"" << setw(2) << setfill('0') << hex << (int)Reg_Store[i].Reg_Val << "\",";
						if (i != NUM_REGS_MAX - 2)
						{
							output_file << dst_stuct.Reg_Mask << " => x\""<< setw(2) << setfill('0') << hex << (int)Reg_Store[i].Reg_Mask << "\"),\n";
						}
						else
						{
							output_file <<dst_stuct.Reg_Mask << " => x\"" << setw(2) << setfill('0') << hex << (int)Reg_Store[i].Reg_Mask << "\") \n";	
						}					
				}
		output_file << "\t\t);" << endl;
		output_file << "end package body " << dst_package_name << ";" << endl;

	cout << "Successful conversion" << endl;
	cout << "output path : " << out_file_name_tol << endl;
	output_file.close();
#pragma endregion

#pragma region Show Result?
		cout << "Do you want show results in the next lines ? yes -> y ,no -> any character" << endl;
		cin >> in_str_of_show_result;
		cout << "===================================================" << endl;
		cout << "===================   result     ===================" << endl;
		cout << "===================================================" << endl;
		ifstream input_file(out_file_name_tol, std::ios::in);
		string input_file_str;
		if (in_str_of_show_result == "y")
		{
			if (input_file) {
				while (!input_file.eof()) input_file_str.push_back(input_file.get());
			}
			std::cout << input_file_str << endl;
			input_file.close();
		}
		cout << "===================================================" << endl;
		cout << "================= end of program  =================" << endl;
		cout << "========== please cheak output file  =============" << endl;
		cout << "===================================================" << endl;
#pragma endregion
//----------------------------------------------------------------	
	system("pause");
    return 0;
}